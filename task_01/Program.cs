﻿using System;
using System.Threading;

namespace task_01
{
    class MainClass
    {
        static readonly Random rnd = new Random();

        public static void Main()
        {
            Renderer.InitCanvas();

            for (int i = 0; i < Renderer.ScreenWidth; i++)
            {
                new Thread(HandleChain).Start(i);
            }
        }

        static void HandleChain(object x)
        {
            if (x.GetType() != typeof(int)) {
                throw new ArgumentException("int type expected as x position");
            };

            Chain chain = new Chain((int)x, rnd.Next(4, 10));

            Thread.Sleep(rnd.Next(1000, 4000));

            while (true)
            {
                if (chain.Y - chain.Length == Renderer.ScreenHeight)
                {
                    chain = new Chain((int)x, rnd.Next(4, 10));
                    Thread.Sleep(rnd.Next(1000, 5000));
                }

                Renderer.PrintChain(chain);

                chain.Next();

                Thread.Sleep(100);
            }
        }
    }
}

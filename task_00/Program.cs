﻿using System;
using System.Threading;

namespace task_00
{
    class MainClass
    {
        static int depth = 0;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Recursion();
        }

        static void Recursion() {
            Thread thread = new Thread(Recursion);

            thread.Name = String.Format("Thread {0}", depth++);

            Thread.Sleep(1000);

            Console.WriteLine(thread.Name);

            thread.Start();
        }
    }
}

﻿using System;
namespace task_01
{
    public static class Renderer
    {
		static Object lockObj = new Object();

        public static readonly int ScreenWidth = Console.BufferWidth;
        public static readonly int ScreenHeight = Console.BufferHeight;

        public static void InitCanvas()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorVisible = false;

            for (int i = 0; i < ScreenWidth; i++)
            {
                for (int j = 0; j < ScreenHeight; j++)
                {
                    Print(' ', i, j);
                }
            }
        }

        public static void Print(char data, int x, int y, ConsoleColor color = ConsoleColor.DarkGreen)
        {
            if (y < 0 || y >= ScreenHeight) return;
            if (x < 0 || x >= ScreenWidth) return;

            lock (lockObj)
            {
                Console.ForegroundColor = color;
                Console.SetCursorPosition(x, y);
                Console.Write(data);
            }
        }

        public static void PrintChain(Chain chain)
        {
            Print(' ', chain.X, chain.Y - chain.Length);
            Print(chain[0], chain.X, chain.Y, ConsoleColor.White);
            Print(chain[1], chain.X, chain.Y - 1, ConsoleColor.Green);

            for (int i = 2; i < chain.Length; i++)
            {
                Print(chain[i], chain.X, chain.Y - i);
            }
        }
    }
}

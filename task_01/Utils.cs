﻿using System;
namespace task_01
{
    public static class Utils
    {
        static readonly char[] ALLOWED_CHARS = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
        static readonly Random rnd = new Random();

        public static char GetRandomChar()
        {
            int i = rnd.Next(0, ALLOWED_CHARS.Length - 1);

            return ALLOWED_CHARS[i];
        }
    }
}

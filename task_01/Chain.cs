﻿namespace task_01
{
    public class Chain 
    {
        public int X { get; }
        public int Y { get; set; }
        public int Length { get; set; }

        public Chain(int x, int size)
        {
            X = x;
            Y = 0;

            Length = size;
        }

        public char this[int index]
        {
            get { return Utils.GetRandomChar(); }
        }

        public void Next()
        {
            Y++;
        }
    }
}
